
/*
var colors=['red','green','yellow'];


colors.forEach(function(color){ 
console.log(color);
}
);
*/
/*one way
var number=[5,7,9,11,8];
var sum=0;
number.forEach(function(numbers){
sum+=numbers;
})
console.log(sum);
*/

/*
var number=[5,7,9,11,8,8];
var sum=0;

function add(num){
    sum+=num;
}


number.forEach(add);

console.log(sum);
*/


//////////MAP/////////////////

/*
var number=[2,7,4,11,9];

var newnumber=number.map(function(abc){
    return abc*2;

})
console.log(newnumber);
*/
/*
var cars=[{model:'ford', price:'cheap'},{model:'maruti', price:'costly'}];

var secondprice=cars.map(function(cars){
return cars.price;

});

console.log(secondprice);
*/

/////////////////////Filter///////////////////////

/*
var veg=[{name:'banana', type:'fruit'},
         {name:'mango', type:'fruit'},
         {name:'potato', type:'vegetable'},
         {name:'cucumber', type:'vegetable'},
         {name:'peanut', type:'vegetable'},
         {name:'orange', type:'fruit'}
];


var mgv=veg.filter(function(vegs){
    return vegs.type==='vegetable';
});

console.log(mgv);


*/


//////////////////Find///////////////////
/*
var user=[{name: 'Alex'},{name:'Ram'},{name:'Mani'}];

var abc=user.find(function(auser){
    return auser.name='Ram';
})
console.log(abc);
*/


/*
var posts=[{id:'1', post:'letsgo'},{id:'2', post:'dontgo'},{id:'3',post:'come'}];
var comment={postid:'2', content:'ok not going'};

function postFoComment(posts, comment){
   return posts.find(function(postf){
       return postf.id===comment.postid;
   })
}
var cf=postFoComment(posts, comment);
console.log(cf);
*/

///////////////////////////////every//////////////////////////
/*
var arr=[{model:'dell', size:'32'},{model:'acer', size:'10'},{model:'hp', size:'18'},{model:'hcl', size:'9'}]

var allc=false;
var scom=true;

var al=arr.every(function(co){
    return co.size>16;
})
///false output 
console.log(al);
*/
///////////////////////////////some//////////////////////////

/*
var arr=[{model:'dell', size:'32'},{model:'acer', size:'10'},{model:'hp', size:'18'},{model:'hcl', size:'9'}]

var allc=false;
var scom=true;

var al=arr.some(function(co){
    return co.size>16;
})
///true output 
console.log(al);

*/




//////////////////////////reduce////////////////
/*
var arr=[10,9,7,4,1];
var sum=0

var abc=arr.reduce(function(sum,ar){

    return sum+ar;
},0)

console.log(abc);
*/


///////////////let and const we use instead of var ---we use const when we knot that value is not going to be changed and let if we can change'

///////////////////////////Template String/Template Literal/////////////////////


///////Before ES6////////
/*
function checkD(){
    var abc=new Date().getFullYear();

    return abc;
}
var abcd=checkD();
console.log(abcd);
*/
//////After ES6////////
/*
function checkD(){
    

    return `Date is${new Date().getFullYear()}`;
}
var abcd=checkD();
console.log(abcd);
*/



/////////////////////////***Arrow function***///////////////////////

////////In normal case
/*
var ab=function(a,b){
    return a+b;
}

var cd=ab(4,7);
console.log(cd);
*/
////////////using arrow function we remove functio,n name curly braces if the return type is single line remove retun word
/*
var ab=(a,b)=>a+b;
var bc=ab(11,34);
console.log(bc);
*/
///if only one argument we can remove () bracket also
/*
var ab=b=>2*b;
var bc=ab(11);
console.log(bc);

*/

/////Map with arrow function
/*
var arr=[10,11,4,5];
var cd=arr.map(ab=>ab*2);
console.log(cd);
*/

//////////////taking an object and map
/*
var abc={
    member:['satyam','shivam'],
    team:'bravo',
    teamSum:function(){
        return this.member.map((mm)=>{
            return `${mm} is in team ${this.team}`;
        })
    }
}

var abcd=abc.teamSum();
console.log(abcd);


*/

///////we can further minimise

/*
var abc={
    member:['satyam','rahul '],
    team:'bravo',
    teamSum:function(){
        return this.member.map((mm)=>`${mm} is in team ${this.team}`
        )
    }
}

var abcd=abc.teamSum();
console.log(abcd);
*/


////////////Enhanced object literals//////////
/*

function createL(inventory){

return {

    inventory:inventory,
    inventoryValue:function(){
        return  this.inventory.reduce((total,book)=>total+book.price,0);
    }
,
    priceForTitle: function(title){

        return this.inventory.find( book =>book.title === title).price;
    }

    

};
}

const inventory=[{title: 'Harry Potter', price: 40},{title: 'darogo', price: 60}];


var ab=createL(inventory);
var bc=ab.priceForTitle('darogo');
var cd=ab.inventoryValue();

console.log(ab);
console.log(bc);
console.log(cd);

*/


/////////////////////we can modify in esr like the function word and : from the first code
/*
function createL(inventory){

    return {
    
        inventory,
        inventoryValue(){
            return  this.inventory.reduce((total,book)=>total+book.price,0);
        }
    ,
        priceForTitle(title){
    
            return this.inventory.find( book =>book.title === title).price;
        }
    
        
    
    };
    }
    
    const inventory=[{title: 'Harry Potter', price: 40},{title: 'darogo', price: 60}];
    
    
    var ab=createL(inventory);
    var bc=ab.priceForTitle('darogo');
    var cd=ab.inventoryValue();
    
    console.log(ab);
    console.log(bc);
    console.log(cd);


*/

/////Default functon arguments///////////

/////out put get get
/*
function abc(url,method){
    if(!method){
        method='Get';
    }
    return method;
}

var kk=abc('google','Get');
var kl=abc('google');

console.log(kk);
console.log(kl);
*/

///Also we can write like below
////Output post get post from the call will override the below for this instance

/*
function abc(url,method='Get'){
   
    return method;
    
}

var kk=abc('google','post');
var kl=abc('google');

console.log(kk);
console.log(kl);

*/

//////////////////Rest and Spread/////////////////////

////////In reduce we are adding numbers of array suppose the array was 
///////ab then we were passing abc to add function like this function addn(abc)
///////what if we have no instead array like 2,3,4,5,6

//using rest we can writr the no function using rest operator
///normal casefunction addN(a,b,c,d,e)
///using rest function addN(...num)



////////////////Spread//////////////////////

/*
var ab=['ram','shyam'];
var bc=['hari','om'];
var cd=['babbu','lalu'];


///instead of concat function we can combine all array to one usimg spread

var bv=[...ab ,...bc ,...cd];
console.log(bv);
///oitput (6) ["ram", "shyam", "hari", "om", "babbu", "lalu"]

*/

/*
var abc={
     calC(a,b){

        return a+b;
    }
}

console.log(abc.calC(4,6));

*/


//Another example suppse we were using add method and now we changed to multiply
/*
var abc={
    add(a,b){
        return a+b;
    }
}

//////////using rest we can write like below

var abc={
    //rest
    add(...num){
        //spread
        return this.mul(...num);
    }
,
    mul(a,b){
return a*b;
    }
}

*/


///////////////////Destructuring //////////////////
/*
var abc={
    name:'shivam',
    surname:'singh'
}

var name =abc.name;
var surname=abc.surname;

console.log(name+surname);
*/
//////After restructuring we can minimize the no of line in the code
/*

var abc={
    name:'shivam',
    surname:'singh'
}
///Automatically it will take name and surname from abc but the name should be same 
var {name} =abc;
var {surname}=abc;

console.log(name+surname);
*/

///Array destructuring

/*
const abc=['ram','shyam','lal'];

const [name,name1]=abc;
///below will get shyam ----in case of array destruturing we use []
console.log(name1);

//to print all except first

const [abcd, ...nam]=abc;
console.log(nam);
*/



////////////////////////using both array and object destructuring together///////////////////////////////

/*
const ab = [
    { company: 'googel', locations: 'USA'},
    { company: 'reliance', locations: 'Mumbai'},
    { company: 'HCL', locations: 'noida'}
];

const [{locations}] = ab;

console.log(locations);

*/

///////////////////////Classess/////////////////////////////////



//////////////Before ESR////////////////
/*
function Car(details){
    this.details=details.details;
}

function Toyoto(abcd){
    Car.call(this, abcd);
    this.color=abcd.color;
}

Car.prototype.drive= function(){
    return 'In first vroom';
}

Toyoto.prototype=Object.create(Car.prototype);
Toyoto.prototype.constructor=Toyoto;


Toyoto.prototype.honk=function(){
    return "toyoto honk";
}


var abc=new Toyoto({color:'red',details:'Auto'});
console.log(abc);


console.log(abc.drive());
console.log(abc.honk());

*/
/////////////////Using ESR classes//////////////////////
///////Simple class exam/////////////
/*
class Car{

constructor(details)
{
this.title=details.title;
}

drive(){
return 'from the class';
}
}

///extends for inheritance looks similar to java 
class Toyota extends Car{

    constructor(details){
        super(details);
        this.color=details.color;
    }

    honk(){
        return "lets go";
    }
}


var car=new Toyota({color:'red',title:'daily'});
console.log(car);
console.log(car.drive());
console.log(car.honk());

*/















































































































































































































